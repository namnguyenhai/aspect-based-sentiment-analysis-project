from . import views
from django.urls import path

urlpatterns = [
    path('index', views.check_health, name='index'),
    # path('sa', views.sentiment_analysis_view, name='sentiment_analysis'),
    # path('sa_ui', views.sentiment_analysis_ui, name="sentiment analysis view UI"),
    path('absa', views.aspect_based_sentiment_analysis_view, name="aspect based sentiment analysis"),
    path('absa_ui', views.aspect_based_sentiment_analysis_ui, name="absa ui")
]