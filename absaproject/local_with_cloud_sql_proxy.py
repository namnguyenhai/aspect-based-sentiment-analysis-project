from .settings import *

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        # 'HOST': 'postgresdb-1.cyknwndsdxea.ap-southeast-1.rds.amazonaws.com', # the old port for AWS RDS
        'HOST': 'localhost',  # using cloud_sql_proxy
        'USER': 'postgres',
        'PASSWORD': 'TKTRpeqm&#89',
        # 'PORT': '5432'
    }
}

CREDENTIAL_GOOGLE_PATH = "/home/tekitori/AdditionalSoftwares/G_NLP_key/My Project 78370-ae414fc6a07d.json"