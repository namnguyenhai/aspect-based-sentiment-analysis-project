FROM python:3.7

WORKDIR /usr/src/app

# Set environment variables
ENV PYTHONUNBUFFERED 1

COPY requirements.txt ./

# Install dependencies.
RUN pip install --no-cache-dir -r requirements.txt
RUN apt update
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade tensorflow

COPY . .

EXPOSE 80

CMD [ "python", "manage.py", "runserver", "0.0.0.0:80" ]


# RUN apt-get update && apt-get install -y ca-certificates ffmpeg libsm6 libxext6