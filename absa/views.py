from django.http import JsonResponse, HttpResponse
from absa.sentiment_analysis import analyze_sentiment
from rest_framework.request import Request
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from absa.models import *
from django.shortcuts import render
from nltk.tokenize import sent_tokenize
import nltk
import copy
from absa.topic_modeling import analyze_topic

# nltk.download('punkt')
sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')


@api_view(('GET',))
def check_health(request):
    return JsonResponse({"status": "OK"})


# @api_view(['POST', 'GET'])
# def sentiment_analysis_view(request: Request) -> Response:
#     opinions = request.data['opinions']
#     [Opinion(content=opinion).save() for opinion in opinions]
#     return Response({
#         "returned_results": analyze_sentiment_by_monkeylearn(opinions)
#     }, status=status.HTTP_200_OK)


@api_view(['POST', 'GET'])
def aspect_based_sentiment_analysis_view(request: Request) -> Response:
    paragraph: str = request.data.get('paragraph', None).strip()

    # Process entire document
    entire_doc_result: dict = analyze_sentiment(paragraph)
    entire_doc_result['topic'] = analyze_topic(paragraph)['topic']

    # Use opinion unit extractor
    opinions: list = sent_detector.tokenize(paragraph)
    # Save user's opinions to DB
    [Opinion(content=opinion).save() for opinion in opinions]
    # Sentiment analysis, topic modeling
    list_sentiment_results = [analyze_sentiment(opinion) for opinion in opinions]
    list_topic_results = [analyze_topic(opinion) for opinion in opinions]

    # Combine results, return
    list_opinion_results = copy.deepcopy(list_sentiment_results)
    for i in range(len(list_opinion_results)):
        list_opinion_results[i]['topic'] = list_topic_results[i]['topic']
    return Response({
        "entire_paragraph": entire_doc_result,
        "inside_opinions": list_opinion_results
    }, status=status.HTTP_200_OK)


def aspect_based_sentiment_analysis_ui(req):
    paragraph = req.GET.get('paragraph')
    if paragraph:
        # Process entire document
        entire_doc_result: dict = analyze_sentiment(paragraph)
        entire_doc_result['topic'] = analyze_topic(paragraph)['topic']

        # Use opinion unit extractor
        opinions: list = sent_detector.tokenize(paragraph)

        # Save user's opinions to DB
        [Opinion(content=opinion).save() for opinion in opinions]

        # Sentiment analysis, topic modeling
        list_sentiment_results = [analyze_sentiment(opinion) for opinion in opinions]
        list_topic_results = [analyze_topic(opinion) for opinion in opinions]

        # Combine results, return
        list_opinion_results = copy.deepcopy(list_sentiment_results)
        for i in range(len(list_opinion_results)):
            list_opinion_results[i]['topic'] = list_topic_results[i]['topic']

        # Return with ui
        context = {
            "entire_paragraph": entire_doc_result,
            "inside_opinions": list_opinion_results
        }
        return render(req, 'sa.html', context)
    return render(req, 'sa.html', {})


# def sentiment_analysis_ui(req):
#     if req.method == 'GET':
#         opinion = [req.GET.get('opinion')]
#         print("Opinion {}".format(opinion))
#         if opinion[0] != None:
#             Opinion(content=opinion[0]).save()
#             context = {'result': analyze_sentiment_by_monkeylearn(opinion)}
#             return render(req, 'sa.html', context)
#     return render(req, 'sa.html', {
#         'result': [{}]
#     })
